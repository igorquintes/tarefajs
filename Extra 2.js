function multiplica(num1, num2) {
    if (num2 === 0) {
        return 0;
    }
    else if (num2 > 0) {
        return (num1 + multiplica(num1, num2-1));
    }
    else if (num2 < 0) {
        return -multiplica(num1, -num2);
    }
}

console.log(multiplica(30, -5))
