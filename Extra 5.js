const booksByCategory = [
    {
        category: "Riqueza",
        books: [
            {
                title: "Os segredos da mente milionária",
                author: "T. Harv Eker",
            },
            {
                title: "O homem mais rico da Babilônia",
                author: "George S. Clason",
            },
            {
                title: "Pai rico, pai pobre",
                author: "Robert T. Kiyosaki e Sharon L. Lechter",
            },
        ],
    },
    {
        category: "Inteligência Emocional",
        books: [
            {
                title: "Você é Insubstituível",
                author: "Augusto Cury",
            },
            {
                title: "Ansiedade – Como enfrentar o mal do século",
                author: "Augusto Cury",
            },
            {
                title: "Os 7 hábitos das pessoas altamente eficazes",
                author: "Stephen R. Covey"
            }
        ]
    }
    
];

//Contar o número de categorias e o número de livros em cada categoria

var contadorCategoria = booksByCategory.length;
var contadorLivro = 0;
booksByCategory.forEach(category => {
    contadorLivro += category.books.length;
});
console.log("Número de categorias: " + contadorCategoria);
console.log("Número de livros: " + contadorLivro);

//Contar o número de autores

var autores = [];
booksByCategory.forEach(category => {
    category.books.forEach(livro => {
        if (!autores.includes(livro.author)) {
            autores.push(livro.author);
        }
    });
});
console.log("Número de autores: " + autores.length);

//Mostrar livros do autor Augusto Cury

var livrosAugustoCury = [];
booksByCategory.forEach(category => {
    category.books.forEach(livro => {
        if (livro.author === "Augusto Cury") {
            livrosAugustoCury.push(livro.title);
        }
    });
});
console.log("Livros do Augusto Cury:", livrosAugustoCury);

//Transformar a função acima em uma função que irá receber o nome do autor e devolver os livros desse autor.

function obterLivrosAutor(autor) {
    var livrosAutor = [];
    booksByCategory.forEach(category => {
        category.books.forEach(livro => {
            if (livro.author === autor) {
                livrosAutor.push(livro.title);
            }
        });
    });
    return livrosAutor;
}
console.log("Livros do autor desejado")
console.log(obterLivrosAutor("T. Harv Eker"));
  