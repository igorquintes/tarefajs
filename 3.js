function verificaNotas(nota1, nota2, nota3) {
    let media = (nota1 + nota2 + nota3) / 3;

    if (media >= 60) {
        console.log("Aprovado");
    } else {
        console.log("Reprovado");
    }
}
verificaNotas(20, 80, 100);
verificaNotas(50, 40, 30);