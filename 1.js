function multiplicaMatriz(matrizA, matrizB) {
    var linhaA = matrizA.length, colunaA = matrizA[0].length,
        linhaB = matrizB.length, colunaB = matrizB[0].length,
        C = [];
    if (colunaA != linhaB) return false;
    for (var i = 0; i < linhaA; i++) C[i] = [];
    for (var k = 0; k < colunaB; k++) {
        for (var i = 0; i < linhaA; i++) {
            var t = 0;
            for (var j = 0; j < linhaB; j++) t += matrizA[i][j] * matrizB[j][k];
            C[i][k] = t;
        }
    }
    return C;
}

var matriz_a = [[4, 0], [-1, -1]]
var matriz_b = [[-1, 3], [2, 7]] 

var result = multiplicaMatriz(matriz_a, matriz_b);
console.log(result);
