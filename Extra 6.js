var financeiro = {
    receitas: [20, 30, 50, 10],
    despesas: [15, 10, 30, 20],
    calcularSaldo: function() {
        var renda = 0;
        var gastos = 0;
        for (var i = 0; i < this.receitas.length; i++){
            renda += this.receitas[i];
        }
        for (var i = 0; i < this.despesas.length; i++){
            gastos += this.despesas[i];
        }
        var saldo = renda - gastos;
        if (saldo > 0) {
            console.log("Família tem um saldo positivo de R$" + saldo);
        }else if (saldo < 0){
            console.log("Família tem um saldo negativo de R$" + saldo);
        }else{
            console.log("Família tem um saldo neutro de R$" + saldo);
        }
    }
};
  
financeiro.calcularSaldo();
  
  